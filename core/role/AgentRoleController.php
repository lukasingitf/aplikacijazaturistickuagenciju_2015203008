<?php
    namespace App\Core\Role;

    class AgentRoleController extends \App\Core\Controller {
        public function __pre() {
            if ($this->getSession()->get('agent_id') === null) {
                $this->redirect(\Configuration::BASE . 'login');
            }
        }
    }
