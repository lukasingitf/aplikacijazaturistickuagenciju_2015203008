<?php
    require_once 'Configuration.php';
    require_once 'vendor/autoload.php';
    
    use App\Core\DatabaseConfiguration;
    use App\Core\DatabaseConnection;
   
   // DATABASE CONFIGURATION
    
    $databaseConfiguration = new DatabaseConfiguration(
            Configuration::DATABASE_HOST, 
            Configuration::DATABASE_USER,
            Configuration::DATABASE_PASS,
            Configuration::DATABASE_NAME);
    $databaseConnection = new DatabaseConnection($databaseConfiguration);
    
    $url = strval(filter_input(INPUT_GET, 'URL'));
    $httpMethod = filter_input(INPUT_SERVER, 'REQUEST_METHOD');
    
    // ROUTES
    
    $router = new App\Core\Router();
    
    $routes = require_once 'Routes.php';
    foreach ($routes as $route){
        $router->add($route);
    }
    $route = $router->find($httpMethod, $url);
    $arguments = $route->extractArguments($url);
    
   
    // CONTROLLERS
      
    $fullControlerName = '\\App\\Controllers\\'.$route->getControllerName().'Controller';
    $controller = new $fullControlerName($databaseConnection);
    
    // SESSIONS
    
        $fingerprintProviderFactoryClass  = Configuration::FINGERPRINT_PROVIDER_FACTORY;
	$fingerprintProviderFactoryMethod = Configuration::FINGERPRINT_PROVIDER_METHOD;
	$fingerprintProviderFactoryArgs   = Configuration::FINGERPRINT_PROVIDER_ARGS;
	$fingerprintProviderFactory = new $fingerprintProviderFactoryClass;
	$fingerprintProvider = $fingerprintProviderFactory->$fingerprintProviderFactoryMethod(...$fingerprintProviderFactoryArgs);

	$sessionStorageClassName = Configuration::SESSION_STORAGE;
	$sessionStorageConstructorArguments = Configuration::SESSION_STORAGE_DATA;
	$sessionStorage = new $sessionStorageClassName(...$sessionStorageConstructorArguments);

	$session = new \App\Core\Session\Session($sessionStorage, Configuration::SESSION_LIFETIME);
	$session->setFingerprintProvider($fingerprintProvider);

	$controller->setSession($session);
	$controller->getSession()->reload();
	$controller->__pre();
        $controller->getSessionAgentId();
        
    // CONTROLLERS DATA
        
    call_user_func_array([$controller, $route->getMethodName()],$arguments);
    
     $controller->getSession()->save();
    
    $data = $controller->getData();
    
//    print_r($data);
//    exit;
    
//    foreach ($data as $name => $value) {
//        $$name = $value;
//    
//    }
//  
//    require_once 'views/'.$route->getControllerName().'/'.$route->getMethodName().'.php';
  
    // TWIG
    
    $loader = new Twig_Loader_Filesystem("./views");
    $twig = new Twig_Environment($loader, ["cache"=>"./twig-cache","auto_reload"=>true]);
    $data['BASE']= Configuration::BASE;
    $html = $twig->render($route->getControllerName().'/'.$route->getMethodName().'.html', $data);
    
    echo $html;
    
    
    