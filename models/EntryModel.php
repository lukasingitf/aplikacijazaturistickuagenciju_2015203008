<?php

namespace App\Models;

use App\Core\Field;
use App\Validators\NumberValidator;
use App\Validators\DateTimeValidator;

class EntryModel extends \App\Core\Model {

    protected function getFields(): array {
        return [
            'entry_id' => new Field((new NumberValidator())->setIntegerLength(10), FALSE),
            'package_id' => new Field((new NumberValidator())->setIntegerLength(10)),
            'agent_id' => new Field((new NumberValidator())->setIntegerLength(10)),
            'client_id' => new Field((new NumberValidator())->setIntegerLength(10)),
            'created_at' => new Field((new DateTimeValidator())->allowDate()->allowTime(), FALSE)
        ];
    }

    public function showAllByClient(int $id) {
        $sql = 'SELECT * from((( entry INNER JOIN client on entry.client_id=client.client_id)INNER JOIN agent on entry.agent_id=agent.agent_id) INNER JOIN package on entry.package_id=package.package_id) WHERE entry.client_id=?;';
        $prep = $this->getConnection()->prepare($sql);
        $res = $prep->execute([$id]);
        $packages = NULL;
        if ($res) {
            $packages = $prep->fetchAll(\PDO::FETCH_OBJ);
        }
        return $packages;
    }

}
