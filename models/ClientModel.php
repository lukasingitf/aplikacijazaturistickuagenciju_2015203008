<?php
    namespace App\Models;
    use App\Core\Field;
    use App\Validators\BitValidator;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\JmbgValidator;
    use App\Validators\EmailValidator;
    use App\Validators\PhoneValidator;
    use App\Validators\PassportValidator;
    use App\Validators\StringValidator;
    



    class ClientModel extends \App\Core\Model {

        protected function getFields(): array {
            return [
                
                'client_id'=> new Field((new NumberValidator())->setIntegerLength(10) , FALSE),
                'firstname'  => new Field((new StringValidator())->setMaxLength(255)),
                'lastname' => new Field((new StringValidator())->setMaxLength(255)),
                'jmbg' => new Field(new JmbgValidator()),
                'passport' => new Field(new PassportValidator()),
                'address' => new Field((new StringValidator())->setMaxLength(64*1024)),
                'email' => new Field(new EmailValidator()),
                'phone' => new Field(new PhoneValidator()),
                'registered_at' => new Field((new DateTimeValidator())->allowDate()->allowTime(),FALSE)

            ];
        }
    
        public function getByJmbg(float $clientJmbg) {
            return $this->getByFieldName('jmbg', $clientJmbg);
        }
    
    
    

    
    
   

    }
