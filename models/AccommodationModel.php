<?php
    namespace App\Models;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    



    class AccommodationModel extends \App\Core\Model {

        protected function getFields(): array {
            return [
                
                'accommodation_id'=> new Field((new NumberValidator())->setIntegerLength(10) , FALSE),
                'property_name'  => new Field((new StringValidator())->setMaxLength(100)),
                'country_id'=> new Field((new NumberValidator())->setIntegerLength(10))


            ];
        }
        
        

    }
