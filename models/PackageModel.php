<?php

namespace App\Models;

use App\Core\Field;
use App\Validators\BitValidator;
use App\Validators\NumberValidator;
use App\Validators\DateTimeValidator;
use App\Validators\StringValidator;
use App\Validators\TransportationValidator;

class PackageModel extends \App\Core\Model {

    protected function getFields(): array {
        return [
            'package_id' => new Field((new NumberValidator())->setIntegerLength(10), FALSE),
            'is_archived' => new Field(new BitValidator()),
            'title' => new Field((new StringValidator())->setMaxLength(255)),
            'number_of_places' => new Field((new NumberValidator())->setIntegerLength(10)),
            'begins_at' => new Field((new DateTimeValidator())->allowDate()),
            'offer_expires_at' => new Field((new DateTimeValidator())->allowDate()),
            'created_at' => new Field((new DateTimeValidator())->allowDate()->allowTime(), FALSE),
            'description' => new Field((new StringValidator())->setMaxLength(64 * 1024)),
            'price' => new Field((new NumberValidator())->setDecimal()->setUnsigned()->setIntegerLength(7)->setMaxDecimalDigits(2)),
            'accommodation_id' => new Field((new NumberValidator())->setIntegerLength(10)),
            'transportation' => new Field(new TransportationValidator()),
            'number_of_nights' => new Field((new NumberValidator())->setIntegerLength(10)),
            'reservations_count' => new Field((new NumberValidator())->setIntegerLength(10))
        ];
    }

    public function showAllByCoutry(string $country) {
        $sql = 'SELECT * FROM ((package INNER JOIN accommodation on package.accommodation_id = accommodation.accommodation_id) INNER JOIN country on accommodation.country_id=country.country_id) WHERE country.name=? AND package.is_archived=0;';
        $prep = $this->getConnection()->prepare($sql);
        $res = $prep->execute([$country]);
        $packages = [];
        if ($res) {
            $packages = $prep->fetchAll(\PDO::FETCH_OBJ);
        }
        return $packages;
    }
    
    public function showAll() {
        $sql = 'SELECT * FROM ((package INNER JOIN accommodation on package.accommodation_id = accommodation.accommodation_id) INNER JOIN country on accommodation.country_id=country.country_id) WHERE package.is_archived=0;';
        $prep = $this->getConnection()->prepare($sql);
        $res = $prep->execute();
        $packages = [];
        if ($res) {
            $packages = $prep->fetchAll(\PDO::FETCH_OBJ);
        }
        return $packages;
    }
    
    public function showAllArchived() {
        $sql = 'SELECT * FROM ((package INNER JOIN accommodation on package.accommodation_id = accommodation.accommodation_id) INNER JOIN country on accommodation.country_id=country.country_id) WHERE package.is_archived=1;';
        $prep = $this->getConnection()->prepare($sql);
        $res = $prep->execute();
        $packages = [];
        if ($res) {
            $packages = $prep->fetchAll(\PDO::FETCH_OBJ);
        }
        return $packages;
    }
    public function showWholePackage(int $id) {
        $sql = 'select * from ((package INNER JOIN accommodation on package.accommodation_id = accommodation.accommodation_id) INNER JOIN country on accommodation.country_id = country.country_id) WHERE package.package_id=?;';
        $prep = $this->getConnection()->prepare($sql);
        $res = $prep->execute([$id]);
        $package = NULL;
        if ($res) {
            $package = $prep->fetch(\PDO::FETCH_OBJ);
        }
        return $package;
    }
    
    

}
