<?php

namespace App\Models;

use App\Core\Field;
use App\Validators\NumberValidator;
use App\Validators\StringValidator;

class ImageModel extends \App\Core\Model {

    protected function getFields(): array {
        return [
            'image_id' => new Field((new NumberValidator())->setIntegerLength(10), FALSE),
            'path' => new Field((new StringValidator())->setMaxLength(255)),
            'title' => new Field((new StringValidator())->setMaxLength(255)),
            'package_id' => new Field((new NumberValidator())->setIntegerLength(10), FALSE)
           
        ];
    }
}