<?php
    namespace App\Models;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\EmailValidator;
    use App\Validators\StringValidator;
    



    class AgentModel extends \App\Core\Model {

        protected function getFields(): array {
            return [
                
                'agent_id'=> new Field((new NumberValidator())->setIntegerLength(10) , FALSE),
                'fullname'  => new Field((new StringValidator())->setMaxLength(255), FALSE),
                'password_hash' => new Field((new StringValidator())->setMaxLength(255), FALSE),
                'email' => new Field(new EmailValidator(), FALSE),
                'username' => new Field((new StringValidator())->setMaxLength(64), FALSE),

            ];
        }

    }
