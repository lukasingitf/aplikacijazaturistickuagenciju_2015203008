<?php

namespace App\Models;

use App\Core\Field;
use App\Validators\NumberValidator;
use App\Validators\StringValidator;

class CountryModel extends \App\Core\Model {

    protected function getFields(): array {
        return [
            'country_id' => new Field((new NumberValidator())->setIntegerLength(10), FALSE),
            'name' => new Field((new StringValidator())->setMaxLength(255)),
            'amchart_id' => new Field((new StringValidator())->setMaxLength(10)),
            
        ];
    }

    public function getByName(string $countryName) {
        return $this->getByFieldName('name', $countryName);
    }

}
