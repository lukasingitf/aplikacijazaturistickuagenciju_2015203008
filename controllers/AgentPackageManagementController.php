<?php

namespace App\Controllers;

class AgentPackageManagementController extends \App\Core\Role\AgentRoleController {

    public function listAllArchived() {
        $this->updateArchive();

        $packageModel = new \App\Models\PackageModel($this->getDbc());
        $packages = $packageModel->showAllArchived();
        $this->setData('packages', $packages);
    }

    public function listAllActive() {
        $this->updateArchive();

        $packageModel = new \App\Models\PackageModel($this->getDbc());
        $packages = $packageModel->showAll();
        $this->setData('packages', $packages);
    }

    public function getAdd() {
        $accomodationModel = new \App\Models\AccommodationModel($this->getDbc());
        $accommodation = $accomodationModel->getAll();
        $this->setData('accommodation', $accommodation);
    }

    public function postAdd() {
        $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
        $number_of_places = filter_input(INPUT_POST, 'number_of_places', FILTER_SANITIZE_NUMBER_INT);
        $reservations_count = filter_input(INPUT_POST, 'reservations_count', FILTER_SANITIZE_NUMBER_INT);
        $begins_at = filter_input(INPUT_POST, 'begins_at', FILTER_SANITIZE_STRING);
        $offer_expires_at = filter_input(INPUT_POST, 'offer_expires_at', FILTER_SANITIZE_STRING);
        $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
        $price = sprintf("%.2f", filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_FLOAT));
        $accommodation_id = filter_input(INPUT_POST, 'accommodation', FILTER_SANITIZE_NUMBER_INT);
        $transportation = filter_input(INPUT_POST, 'transportation', FILTER_SANITIZE_STRING);
        $number_of_nights = filter_input(INPUT_POST, 'number_of_nights', FILTER_SANITIZE_NUMBER_INT);


        $stringValidator = new \App\Validators\StringValidator();
        if (!$stringValidator->setMinLength(3)->isValid($title)) {
            $this->setData('message', 'Error: title is not valid!');
            return;
        }
        if (!$stringValidator->setMaxLength(3000)->isValid($description)) {
            $this->setData('message', 'Error: description is not valid!');
            return;
        }


        $transportationValidator = new \App\Validators\TransportationValidator();
        if (!$transportationValidator->isValid($transportation)) {
            $this->setData('message', 'Error: transportation is not valid!');
            return;
        }

        $numberValidator = new \App\Validators\NumberValidator();
        if (!$numberValidator->isValid($accommodation_id)) {
            $this->setData('message', 'Error: accommodation is not valid!');
            return;
        }
        if (!$numberValidator->isValid($number_of_places)) {
            $this->setData('message', 'Error: number_of_places is not valid!');
            return;
        }
        if (!preg_match('/\d+/', $reservations_count)) {
            $this->setData('message', 'Error: reservations_count is not valid!');
            return;
        }
        if (!$numberValidator->setMaxDecimalDigits(2)->setDecimal()->isValid($price)) {
            $this->setData('message', 'Error: price is not valid!');
            return;
        }
        if (!$numberValidator->setInteger()->setIntegerLength(10)->isValid($number_of_nights)) {
            $this->setData('message', 'Error: number_of_nights is not valid!');
            return;
        }

        $datetimeValidator = new \App\Validators\DateTimeValidator();
        if (!$datetimeValidator->allowDate()->isValid($begins_at)) {
            $this->setData('message', 'Error: begins_at is not valid!');
            return;
        }
        if (!$datetimeValidator->allowDate()->isValid($offer_expires_at)) {
            $this->setData('message', 'Error: offer_expires_at is not valid!');
            return;
        }

        $packageMododel = new \App\Models\PackageModel($this->getDbc());
        $packageId = $packageMododel->add([
            'title' => $title,
            'number_of_places' => $number_of_places,
            'reservations_count' => $reservations_count,
            'begins_at' => $begins_at,
            'offer_expires_at' => $offer_expires_at,
            'description' => $description,
            'accommodation_id' => $accommodation_id,
            'transportation' => $transportation,
            'number_of_nights' => $number_of_nights,
            'price' => $price
        ]);
        if (!$packageId) {
            $this->setData('message', 'Error: Unable to add package!');
            return;
        }
        if (isset($_FILES['image']) && $_FILES['image']['error'] == 0) {
            $uploadStatus = $this->doImageUpload('image', $packageId);

            if (!$uploadStatus) {
                return;
            }
        }


        $this->redirect(\Configuration::BASE . 'agent/packages');
    }

    public function getEdit($packageId) {

        $this->getAdd();
        $packageModel = new \App\Models\PackageModel($this->getDbc());
        $package = $packageModel->getById($packageId);


        if (!$package) {
            $this->redirect(\Configuration::BASE . 'agent/packages');
        }
        $this->setData('package', $package);
        return $packageModel;
    }

    public function postEdit($packageId) {
        $packageModel = $this->getEdit($packageId);

        $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
        $number_of_places = filter_input(INPUT_POST, 'number_of_places', FILTER_SANITIZE_NUMBER_INT);
        $reservations_count = filter_input(INPUT_POST, 'reservations_count', FILTER_SANITIZE_NUMBER_INT);
        $begins_at = filter_input(INPUT_POST, 'begins_at', FILTER_SANITIZE_STRING);
        $offer_expires_at = filter_input(INPUT_POST, 'offer_expires_at', FILTER_SANITIZE_STRING);
        $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
        $price = sprintf("%.2f", filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING));
        $accommodation_id = filter_input(INPUT_POST, 'accommodation', FILTER_SANITIZE_NUMBER_INT);
        $transportation = filter_input(INPUT_POST, 'transportation', FILTER_SANITIZE_STRING);
        $number_of_nights = filter_input(INPUT_POST, 'number_of_nights', FILTER_SANITIZE_NUMBER_INT);
        $is_archived = filter_input(INPUT_POST, 'is_archived', FILTER_SANITIZE_NUMBER_INT);


        $stringValidator = new \App\Validators\StringValidator();
        if (!$stringValidator->setMinLength(3)->isValid($title)) {
            $this->setData('message', 'Error: title is not valid!');
            return;
        }
        if (!$stringValidator->setMaxLength(3000)->isValid($description)) {
            $this->setData('message', 'Error: description is not valid!');
            return;
        }


        $transportationValidator = new \App\Validators\TransportationValidator();
        if (!$transportationValidator->isValid($transportation)) {
            $this->setData('message', 'Error: transportation is not valid!');
            return;
        }

        $numberValidator = new \App\Validators\NumberValidator();
        if (!$numberValidator->isValid($accommodation_id)) {
            $this->setData('message', 'Error: accommodation is not valid!');
            return;
        }
        if (!$numberValidator->isValid($number_of_places)) {
            $this->setData('message', 'Error: number_of_places is not valid!');
            return;
        }
        if (!preg_match('/\d+/', $reservations_count)) {
            $this->setData('message', 'Error: reservations_count is not valid!');
            return;
        }
        if (!$numberValidator->setMaxDecimalDigits(2)->setDecimal()->isValid($price)) {
            $this->setData('message', 'Error: price is not valid!');
            return;
        }
        if (!$numberValidator->setInteger()->setIntegerLength(10)->isValid($number_of_nights)) {
            $this->setData('message', 'Error: number_of_nights is not valid!');
            return;
        }

        $datetimeValidator = new \App\Validators\DateTimeValidator();
        if (!$datetimeValidator->allowDate()->isValid($begins_at)) {
            $this->setData('message', 'Error: begins_at is not valid!');
            return;
        }
        if (!$datetimeValidator->allowDate()->isValid($offer_expires_at)) {
            $this->setData('message', 'Error: offer_expires_at is not valid!');
            return;
        }
        $boolValidator = new \App\Validators\BitValidator();
        if (!$boolValidator->isValid($is_archived)) {
            $this->setData('message', 'Error: is_archived value is not valid!');
            return;
        }

        $packageModel->editById($packageId, [
            'title' => $title,
            'number_of_places' => $number_of_places,
            'reservations_count' => $reservations_count,
            'begins_at' => $begins_at,
            'offer_expires_at' => $offer_expires_at,
            'description' => $description,
            'accommodation_id' => $accommodation_id,
            'transportation' => $transportation,
            'number_of_nights' => $number_of_nights,
            'price' => $price,
            'is_archived' => $is_archived
        ]);
        if (isset($_FILES['image']) && $_FILES['image']['error'] == 0) {

            $uploadStatus = $this->doImageUpload('image', $packageId);

            if (!$uploadStatus) {
                return;
            }
        }

        $this->redirect(\Configuration::BASE . 'agent/packages');
    }

    private function doImageUpload(string $fieldName, string $fileName): bool {

        unlink(\Configuration::UPLOAD_DIR . $fileName . '.jpg');
        $uploadPath = new \Upload\Storage\FileSystem(\Configuration::UPLOAD_DIR);
        $file = new \Upload\File($fieldName, $uploadPath);
        $file->setName($fileName);
        $file->addValidations([
            new \Upload\Validation\Mimetype("image/jpeg"),
            new \Upload\Validation\Size("3M")
        ]);
        
        try {
            $file->upload();
            return TRUE;
        } catch (Exception $exc) {
            $this->setData('message', 'Error: ' . implode(', ', $file->getErrors()));
            return FALSE;
        }
    }

}
