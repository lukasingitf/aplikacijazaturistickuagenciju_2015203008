<?php

namespace App\Controllers;  

class AgentCountryManagementController  extends \App\Core\Role\AgentRoleController {

    public function listAll() {
        $countryModel = new \App\Models\CountryModel($this->getDbc());
        $countries = $countryModel->getAll();
        $this->setData('countries', $countries);
        
    }
    
    public function getAdd() {
        
    }

     public function postAdd() {
        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
        $amchart_id= filter_input(INPUT_POST, 'amchart_id', FILTER_SANITIZE_STRING);
        
        $stringValidator= new \App\Validators\StringValidator();
        if(!$stringValidator->setMinLength(3)->isValid($name)){
             $this->setData('message', 'Error: Country name is not valid!');
             return;
        }
        
        if(!$stringValidator->setMinLength(2)->isValid($amchart_id)){
             $this->setData('message', 'Error: Amchart_id is not valid!');
             return;
        }
        
        
        
        $countryModel = new \App\Models\CountryModel($this->getDbc());
        $countryId = $countryModel->add([
            'name' => $name,
            'amchart_id' => $amchart_id,
        ]);        
        if(!$countryId){
            $this->setData('message', 'Error: Unable to add country!');
            return;
        }        
        $this->redirect(\Configuration::BASE.'agent/countries');
        
     }
     
     
     public function getEdit($countryId) {
       $countryModel = new \App\Models\CountryModel($this->getDbc());
         $country = $countryModel->getById($countryId);

        if (!$country) {
            $this->redirect(\Configuration::BASE . 'agent/countries');
        }
        $this->setData('country', $country);
        return $countryModel;
    } 
    
    public function postEdit($countryId) {
        $countryModel = $this->getEdit($countryId);
        
        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
        $amchart_id= filter_input(INPUT_POST, 'amchart_id', FILTER_SANITIZE_STRING);
        
        $stringValidator= new \App\Validators\StringValidator();
        if(!$stringValidator->setMinLength(3)->isValid($name)){
             $this->setData('message', 'Error: Cpuntry name is not valid!');
             return;
        }
        
        if(!$stringValidator->setMinLength(2)->isValid($amchart_id)){
             $this->setData('message', 'Error: Amchart_id is not valid!');
             return;
        }
        
        $countryId = $countryModel->editById($countryId,[
            'name' => $name,
            'amchart_id' => $amchart_id,
        ]);        
        if(!$countryId){
            $this->setData('message', 'Error: Unable to edit country!');
            return;
        }        
        $this->redirect(\Configuration::BASE.'agent/countries');
        
    }
    
    public function getDelete($countryId){
         $countryModel = new  \App\Models\CountryModel($this->getDbc());
         $accommodationModel = new \App\Models\AccommodationModel($this->getDbc());
         
         $accommodation = $accommodationModel->getByFieldName('country_id', $countryId);
         
         if(!$accommodation){
             $countryModel->deleteById($countryId);
             $this->redirect(\Configuration::BASE . 'agent/countries');
             return;
         }
         
         $this->setData('message', 'Error: Cannot delete country. Country is used for accommodation!');
    }
    
    
}
