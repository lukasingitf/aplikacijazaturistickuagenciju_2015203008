<?php
    namespace App\Controllers;
    
        class PackageController extends \App\Core\Controller {
           
            
            public function listAll(){
                
                $this->updateArchive();
                
                $packageModel = new \App\Models\PackageModel($this->getDbc());
                $packages = $packageModel->showAll();
                $this->setData('packages', $packages); 
                
          
            }
            
            public function show($id){
                
                $this->updateArchive();

                $packageModel = new \App\Models\PackageModel($this->getDbc());
                $package = $packageModel->showWholePackage($id);               
                $this->setData('package', $package);
                
            }
            
           

        } 