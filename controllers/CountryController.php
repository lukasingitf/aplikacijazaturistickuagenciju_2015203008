<?php

namespace App\Controllers;

class CountryController extends \App\Core\Controller {

    public function show($countryName) {
        
        $this->updateArchive();
        
        $countryModel = new \App\Models\CountryModel($this->getDbc());
        $country = $countryModel->getByName($countryName);

        if (!$country) {
            $this->redirect(\Configuration::BASE);
        }
        $this->setData('country', $country);
        
        $packageModel = new \App\Models\PackageModel($this->getDbc());
        $packages = $packageModel->showAllByCoutry($countryName);
        
        if(!$packages) {
             $this->redirect(\Configuration::BASE);
        }
         $this->setData('packages', $packages);
        
        
    }

}
