<?php

namespace App\Controllers;

class AgentClientManagementController  extends \App\Core\Role\AgentRoleController {

    public function clients() {
        $clientModel = new \App\Models\ClientModel($this->getDbc());
        $clients = $clientModel->getAll();
        $this->setData('clients', $clients);
    }

    public function getEdit($clientId) {
        $clientModel = new \App\Models\ClientModel($this->getDbc());
        $client = $clientModel->getById($clientId);

        if (!$client) {
            $this->redirect(\Configuration::BASE . 'agent/clients');
        }
        $this->setData('client', $client);
        return $clientModel;
    }

    


    public function postEdit($clientId) {
        $clientModel = $this->getEdit($clientId);
        
        $firstname = filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING);
        $lastname = filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING);
        $jmbg = filter_input(INPUT_POST, 'jmbg', FILTER_SANITIZE_NUMBER_FLOAT);
        $passport = filter_input(INPUT_POST, 'passport', FILTER_SANITIZE_NUMBER_INT);
        $address = filter_input(INPUT_POST, 'address', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
        $phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING);
        
         $stringValidator= new \App\Validators\StringValidator();
        if(!$stringValidator->setMinLength(3)->isValid($firstname)){
             $this->setData('message', 'Error: Firstname is not valid!');
             return;
        }
        
        if(!$stringValidator->setMinLength(3)->isValid($lastname)){
             $this->setData('message', 'Error: Lastname is not valid!');
             return;
        }
        
        $jmbgValidator = new \App\Validators\JmbgValidator();
        if(!$jmbgValidator->isValid($jmbg)){
            $this->setData('message', 'Error: Personal ID is not valid!');
             return;
        }
        
        $passportValidator = new \App\Validators\PassportValidator();
        if(!$passportValidator->isValid($passport)){
            $this->setData('message', 'Error: Passport number is not valid!');
             return;
        }
        
        if(!$stringValidator->setMinLength(5)->isValid($address)){
             $this->setData('message', 'Error: Address is not valid!');
             return;
        }
        
        $emailValidator=new \App\Validators\EmailValidator();
        if(!$emailValidator->isValid($email)){
             $this->setData('message', 'Error: Email is not valid!');
             return;
        }
        
        $phoneValidator = new \App\Validators\PhoneValidator();
         if(!$phoneValidator->isValid($phone)){
             $this->setData('message', 'Error: Phone is not valid!');
             return;
        }

        $clientModel->editById($clientId, [
            'firstname' => $firstname,
            'lastname' => $lastname,
            'jmbg' => $jmbg,
            'passport' => $passport,
            'address' => $address,
            'email' => $email,
            'phone' => $phone
        ]);

        $this->redirect(\Configuration::BASE . 'agent/clients');
    }
    
    public function getAdd() {
        
    }
    
    public function postAdd() {
        $firstname = filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING);
        $lastname = filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING);
        $jmbg = filter_input(INPUT_POST, 'jmbg', FILTER_SANITIZE_NUMBER_FLOAT);
        $passport = filter_input(INPUT_POST, 'passport', FILTER_SANITIZE_NUMBER_INT);
        $address = filter_input(INPUT_POST, 'address', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
        $phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING);
        
        $stringValidator= new \App\Validators\StringValidator();
        if(!$stringValidator->setMinLength(3)->isValid($firstname)){
             $this->setData('message', 'Error: Firstname is not valid!');
             return;
        }
        
        if(!$stringValidator->setMinLength(3)->isValid($lastname)){
             $this->setData('message', 'Error: Lastname is not valid!');
             return;
        }
        
        $jmbgValidator = new \App\Validators\JmbgValidator();
        if(!$jmbgValidator->isValid($jmbg)){
            $this->setData('message', 'Error: Personal ID is not valid!');
             return;
        }
        
        $passportValidator = new \App\Validators\PassportValidator();
        if(!$passportValidator->isValid($passport)){
            $this->setData('message', 'Error: Passport number is not valid!');
             return;
        }
        
        if(!$stringValidator->setMinLength(5)->isValid($address)){
             $this->setData('message', 'Error: Address is not valid!');
             return;
        }
        
        $emailValidator=new \App\Validators\EmailValidator();
        if(!$emailValidator->isValid($email)){
             $this->setData('message', 'Error: Email is not valid!');
             return;
        }
        
        $phoneValidator = new \App\Validators\PhoneValidator();
         if(!$phoneValidator->isValid($phone)){
             $this->setData('message', 'Error: Phone is not valid!');
             return;
        }
        
        
        
        $clientModel = new \App\Models\ClientModel($this->getDbc());
        $clientId = $clientModel->add([
            'firstname' => $firstname,
            'lastname' => $lastname,
            'jmbg' => $jmbg,
            'passport' => $passport,
            'address' => $address,
            'email' => $email,
            'phone' => $phone
        ]);        
        if(!$clientId){
            $this->setData('message', 'Error: Unable to add client!');
            return;
        }        
        $this->redirect(\Configuration::BASE.'agent/clients');

        
        
        
        
        
        
        
    }

}
