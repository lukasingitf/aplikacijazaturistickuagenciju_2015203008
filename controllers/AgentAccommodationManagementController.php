<?php

namespace App\Controllers;

class AgentAccommodationManagementController  extends \App\Core\Role\AgentRoleController {

    public function listAll() {
        $accommodationModel = new \App\Models\AccommodationModel($this->getDbc());
        $accommodations = $accommodationModel->selectInnerJoinTwo('country','country_id');
        $this->setData('accommodations', $accommodations);
        
    }
    
    public function getAdd() {
         $countryModel = new \App\Models\CountryModel($this->getDbc());
         $countries = $countryModel->getAll();
         $this->setData('countries', $countries);
    }

     public function postAdd() {
        $propertyName = filter_input(INPUT_POST, 'property_name', FILTER_SANITIZE_STRING);
        $countryId= filter_input(INPUT_POST, 'country_id', FILTER_SANITIZE_NUMBER_INT);
      
        $stringValidator= new \App\Validators\StringValidator();
        if(!$stringValidator->setMinLength(3)->isValid($propertyName)){
             $this->setData('message', 'Error: property name name is not valid!');
             return;
        }
        
        $numberValidator= new \App\Validators\NumberValidator();
        if (!$numberValidator->isValid($countryId)){
            $this->setData('message', 'Please choose country!');
            return;
        }
        
        $accommodationModel = new \App\Models\AccommodationModel($this->getDbc());

        $accommodationId = $accommodationModel->add([
            'property_name' => $propertyName,
            'country_id' => $countryId,
        ]);        
        if(!$accommodationId){
            $this->setData('message', 'Error: Unable to add accommodation!');
            return;
        }        
        $this->redirect(\Configuration::BASE.'agent/accommodation');
        
     }
     
     
     public function getEdit($accommodationId) {
         $this->getAdd();
         $accommodationModel = new \App\Models\AccommodationModel($this->getDbc());
         $accommodation= $accommodationModel->getById($accommodationId);

        if (!$accommodation) {
            $this->redirect(\Configuration::BASE . 'agent/accommodation');
        }
        $this->setData('accommodation', $accommodation);
        return $accommodationModel;
    } 
    
    public function postEdit($accommodationId) {
        $accommodationModel = $this->getEdit($accommodationId);
        
        $propertyName = filter_input(INPUT_POST, 'property_name', FILTER_SANITIZE_STRING);
        $countryId= filter_input(INPUT_POST, 'country_id', FILTER_SANITIZE_NUMBER_INT);
      
        $stringValidator= new \App\Validators\StringValidator();
        if(!$stringValidator->setMinLength(3)->isValid($propertyName)){
             $this->setData('message', 'Error: property name name is not valid!');
             return;
        }
        
        $numberValidator= new \App\Validators\NumberValidator();
        if (!$numberValidator->isValid($countryId)){
            $this->setData('message', 'Please choose country!');
            return;
        }
        
        $accommodationIdcheck = $accommodationModel->editById($accommodationId,[
            'property_name' => $propertyName,
            'country_id' => $countryId
        ]);        
        if(!$accommodationIdcheck){
            $this->setData('message', 'Error: Unable to edit accommodationId!');
            return;
        }        
        $this->redirect(\Configuration::BASE.'agent/accommodation');
        
    }
    
    public function getDelete($accommodationId){
         $packageModel = new  \App\Models\PackageModel($this->getDbc());
         $accommodationModel = new \App\Models\AccommodationModel($this->getDbc());
       
         $packege = $packageModel->getByFieldName('accommodation_id', $accommodationId);
         
         if(!$packege){
             $accommodationModel->deleteById($accommodationId);
             $this->redirect(\Configuration::BASE . 'agent/accommodation');
             return;
         }
         
         $this->setData('message', 'Error: Cannot delete accommodation. Accommodation is used for package!');
    }
    
    
}
