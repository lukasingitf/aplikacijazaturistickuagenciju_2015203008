<?php

namespace App\Controllers;

class AgentEntryManagementController extends \App\Core\Role\AgentRoleController {

    public function listByClient(int $id) {
$this->updateArchive();

        $entryModel = new \App\Models\EntryModel($this->getDbc());
        $entries = $entryModel->showAllByClient($id);
        $this->setData('entries', $entries);
    }

    public function getAdd() {
        $this->updateArchive();

        $packageModel = new \App\Models\PackageModel($this->getDbc());
        $packages = $packageModel->showAll();
        $this->setData('packages', $packages);

        $clientModel = new \App\Models\ClientModel($this->getDbc());
        $clients = $clientModel->getAll();
        $this->setData('clients', $clients);
    }

    public function postAdd() {



        $packageId = filter_input(INPUT_POST, 'package_id', FILTER_SANITIZE_NUMBER_INT);
        $clientId = filter_input(INPUT_POST, 'client_id', FILTER_SANITIZE_NUMBER_INT);
        $agent = $this->getSession()->get('agent_id');

        $numberValidator = new \App\Validators\NumberValidator();
        if (!$numberValidator->isValid($packageId)) {
            $this->setData('message', 'Error: package is not valid!');
            return;
        }
        if (!$numberValidator->isValid($clientId)) {
            $this->setData('message', 'Error: client is not valid!');
            return;
        }
        if (!$numberValidator->isValid($agent)) {
            $this->setData('message', 'Error: agent is not valid!');
            return;
        }

        $packageModel = new \App\Models\PackageModel($this->getDbc());
        $package = $packageModel->getById($packageId);

        if ($package->number_of_places <= $package->reservations_count) {
            $this->setData('message', 'Error: There is no more places left for selected package!');
            return;
        }

        $entryModel = new \App\Models\EntryModel($this->getDbc());
        $entries = $entryModel->getAll();


        foreach ($entries as $entry) {
            if ($entry->client_id == $clientId && $entry->package_id == $packageId) {
                $this->setData('message', 'Error: Selected client has already been asigned for this packege!');
                return;
            }
        }

        $entryId = $entryModel->add([
            'package_id' => $packageId,
            'client_id' => $clientId,
            'agent_id' => $agent
        ]);
        
        if (!$entryId) {
            $this->setData('message', 'Error: Unable to make entry!');
            return;
        }
        
        $packageModel->editById($packageId, [
            
            'reservations_count' => $package->reservations_count + 1,
            
        ]);
        
        $this->redirect(\Configuration::BASE . 'agent/clients');
    }
    
    public function getDelete($entryId){
        
        $packageModel = new \App\Models\PackageModel($this->getDbc());
        $entryModel = new \App\Models\EntryModel($this->getDbc());
        
        $entry = $entryModel->getById($entryId);
        
        $entryModel->deleteById($entryId);
        $package = $packageModel->getById($entry->package_id);
        $currentReservationsCount = $package->reservations_count;
        $packageModel->editById($entry->package_id, ['reservations_count'=>$currentReservationsCount-1]);
        
        $this->redirect(\Configuration::BASE . 'agent/clients');
        
    }

}
