<?php

namespace App\Controllers;

class MainController extends \App\Core\Controller {

//            public function home(){
//                $countryModel = new \App\Models\CountryModel($this->getDbc());
//                $countries = $countryModel->getAll();
//                $this->setData('countries', $countries);
//          
//            }
    public function home() {
        $this->updateArchive();

        $packageModel = new \App\Models\PackageModel($this->getDbc());
        $packages = $packageModel->showAll();
        $this->setData('packages', $packages);
    }

    public function getLogin() {
//        if($this->getSession()->get('agent_id')>0){
//            $this->redirect(\Configuration::BASE . 'agent/dashboard');
//        }
//        $this->getSession()->clear();
       
    }

    public function postLogin() {
        $username = \filter_input(INPUT_POST, 'login_username', FILTER_SANITIZE_STRING);
        $password = \filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_STRING);

        $validPassword = (new \App\Validators\StringValidator())->setMinLength(7)->setMaxLength(120)->isValid($password);
        if (!$validPassword) {
            $this->setData('message', 'Login failed! Your password is not valid.');
            return;
        }

        $agentModel = new \App\Models\AgentModel($this->getDbc());
        $agent = $agentModel->getByFieldName('username', $username);
        if (!$agent) {
            $this->setData('message', 'Login failed! Username doesn\'t exist.');
            return;
        }

        $passwordHash = $agent->password_hash;

        if (!password_verify($password, $passwordHash)) {
            $this->setData('message', 'Login failed! Password doesn\'t match.');
            return;
        }

        $this->getSession()->put('agent_id', $agent->agent_id);
        $this->getSession()->save();

        $this->redirect(\Configuration::BASE . 'agent/dashboard');
    }

    public function getLogout() {
        $this->getSession()->remove('agent_id');
     
        $this->getSession()->save();
     
        $this->redirect(\Configuration::BASE . 'login');
    }

}
