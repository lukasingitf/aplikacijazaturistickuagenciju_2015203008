    function validateClientForm() {
    let status = true;

    document.querySelector('#error-message').classList.add('d-none');
    document.querySelector('#error-message').innerHTML = '';
   
    const firstname = document.querySelector('#firstname').value;
    if (!firstname.match(/([A-Z][a-z]{2,})+/)) {
        document.querySelector('#error-message').innerHTML += 'Firstname is not valid, must begin with capital letter and containt at least 2 letters...<br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    
    const lastname = document.querySelector('#lastname').value;
    if (!lastname.match(/^[A-Z][a-z]{1,}.([A-Z][a-z]{1,})*$/)) {
        document.querySelector('#error-message').innerHTML += 'Lastname is not valid, must begin with capital letter and containt at least 2 letters...<br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    
    const jmbg = document.querySelector('#jmbg').value;
    if (!jmbg.match(/^[0-3][0-9][0-1][0-9][09]\d{8}$/)) {
        document.querySelector('#error-message').innerHTML += 'Personal ID is not valid... <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    const passport = document.querySelector('#passport').value;
    if (!passport.match(/^\d{9}$/)) {
        document.querySelector('#error-message').innerHTML += 'Passport number is not valid... <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    const address = document.querySelector('#address').value;
    if (!address.match(/.*[^\s]{3,}.*/)) {
        document.querySelector('#error-message').innerHTML += 'Address is not valid, must contain at least 3 letters... <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    const email = document.querySelector('#email').value;
    if (!email.match(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)) {
        document.querySelector('#error-message').innerHTML += 'Email is not valid... <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    const phone = document.querySelector('#phone').value;
    if (!phone.match(/^^\+\d{9,}$/)) {
        document.querySelector('#error-message').innerHTML += 'Phone number is not valid, it must be in format +(country code)63111222... <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    const description = document.getElementById('#description').value;
    if (!description.match(/.*[^\s]{3,}.*/)) {
        document.querySelector('#error-message').innerHTML += 'Description is not valid here <br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    
    return status;
}
    
