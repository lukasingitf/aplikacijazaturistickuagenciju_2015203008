<?php
return[
    //App\Core\Route::get('|^client/([0-9]+)/?$|', 'Client', 'show'),
    
    # Login
    App\Core\Route::get('|^login/?$|', 'Main', 'getLogin'),
    App\Core\Route::post('|^login/?$|', 'Main', 'postLogin'),
    App\Core\Route::get('|^agent/logout/?$|', 'Main', 'getLogout'),
    
    
    
    #Agent role routes:
    
    App\Core\Route::get('|^agent/dashboard/?$|', 'AgentDashboard', 'index'), 
    
    # Agent clients
    App\Core\Route::get('|^agent/clients/?$|', 'AgentClientManagement', 'clients'), 
    App\Core\Route::get('|^agent/clients/add/?$|', 'AgentClientManagement', 'getAdd'),
    App\Core\Route::post('|^agent/clients/add/?$|', 'AgentClientManagement', 'postAdd'),
    App\Core\Route::get('|^agent/clients/edit/([0-9]+)/?$|', 'AgentClientManagement', 'getEdit'),
    App\Core\Route::post('|^agent/clients/edit/([0-9]+)/?$|', 'AgentClientManagement', 'postEdit'),
    # Agent entries
    App\Core\Route::get('|^agent/entries/?$|', 'AgentEntryManagement', 'listAll'), 
    App\Core\Route::get('|^agent/entries/client/([0-9]+)/?$|', 'AgentEntryManagement', 'listByClient'), 
    App\Core\Route::get('|^agent/entries/add/?$|', 'AgentEntryManagement', 'getAdd'),
    App\Core\Route::post('|^agent/entries/add/?$|', 'AgentEntryManagement', 'postAdd'),
    App\Core\Route::get('|^agent/entries/edit/([0-9]+)/?$|', 'AgentEntryManagement', 'getEdit'),
    App\Core\Route::post('|^agent/entries/edit/([0-9]+)/?$|', 'AgentEntryManagement', 'postEdit'),
    App\Core\Route::get('|^agent/entries/delete/([0-9]+)/?$|', 'AgentEntryManagement', 'getDelete'),
    
    
    # Agent packages
    App\Core\Route::get('|^agent/packages/?$|', 'AgentPackageManagement', 'listAllActive'), 
    App\Core\Route::get('|^agent/packages/archived/?$|', 'AgentPackageManagement', 'listAllArchived'), 
    App\Core\Route::get('|^agent/packages/add/?$|', 'AgentPackageManagement', 'getAdd'),
    App\Core\Route::post('|^agent/packages/add/?$|', 'AgentPackageManagement', 'postAdd'),
    App\Core\Route::get('|^agent/package/edit/([0-9]+)/?$|', 'AgentPackageManagement', 'getEdit'),
    App\Core\Route::post('|^agent/package/edit/([0-9]+)/?$|', 'AgentPackageManagement', 'postEdit'),
   
    # Agent countries
    App\Core\Route::get('|^agent/countries/?$|', 'AgentCountryManagement', 'listAll'), 
    App\Core\Route::get('|^agent/countries/add/?$|', 'AgentCountryManagement', 'getAdd'),
    App\Core\Route::post('|^agent/countries/add/?$|', 'AgentCountryManagement', 'postAdd'),
    App\Core\Route::get('|^agent/countries/edit/([0-9]+)/?$|', 'AgentCountryManagement', 'getEdit'),
    App\Core\Route::post('|^agent/countries/edit/([0-9]+)/?$|', 'AgentCountryManagement', 'postEdit'),
    App\Core\Route::get('|^agent/countries/delete/([0-9]+)/?$|', 'AgentCountryManagement', 'getDelete'),
    # Agent accommodations
    App\Core\Route::get('|^agent/accommodation/?$|', 'AgentAccommodationManagement', 'listAll'), 
    App\Core\Route::get('|^agent/accommodation/add/?$|', 'AgentAccommodationManagement', 'getAdd'),
    App\Core\Route::post('|^agent/accommodation/add/?$|', 'AgentAccommodationManagement', 'postAdd'),
    App\Core\Route::get('|^agent/accommodation/edit/([0-9]+)/?$|', 'AgentAccommodationManagement', 'getEdit'),
    App\Core\Route::post('|^agent/accommodation/edit/([0-9]+)/?$|', 'AgentAccommodationManagement', 'postEdit'),
    App\Core\Route::get('|^agent/accommodation/delete/([0-9]+)/?$|', 'AgentAccommodationManagement', 'getDelete'),
    
    

    # Public
    App\Core\Route::get('|^country/([a-zA-Z]+)/?$|', 'Country', 'show'),
    
    App\Core\Route::get('|^packages/?$|', 'Package', 'listAll'),
    App\Core\Route::get('|^package/([0-9]+)/?$|', 'Package', 'show'),
    

    
    App\Core\Route::any('|^.*$|', 'Main', 'home')
];
