<?php

namespace App\Validators;

use \App\Core\Validator;

class TransportationValidator implements Validator {

    public function isValid(string $value): bool {

        switch ($value) {
            case 'SELF TRANSPORT':
                return TRUE;

            case 'AIRPLANE':

                return TRUE;
            case 'BUS':

                return TRUE;
            case 'MINIBUS':

                return TRUE;

        }
        return FALSE;
    }

}
