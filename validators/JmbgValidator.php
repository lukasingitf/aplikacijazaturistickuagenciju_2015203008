<?php
    namespace App\Validators;

    use \App\Core\Validator;

    class JmbgValidator implements Validator {
        public function isValid(string $value): bool {
            return \boolval(\preg_match('/^[0-3][0-9][0-1][0-9][09]\d{8}$/', $value));
        }
    }
