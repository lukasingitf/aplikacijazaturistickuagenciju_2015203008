<?php
    namespace App\Validators;

    use \App\Core\Validator;

    class PassportValidator implements Validator {
        public function isValid(string $value): bool {
            return \boolval(\preg_match('|^\d{9}$|', $value));
        }
    }
