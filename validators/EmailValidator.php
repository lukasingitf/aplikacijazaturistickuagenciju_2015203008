<?php
    namespace App\Validators;

    use \App\Core\Validator;

    class EmailValidator implements Validator {
        public function isValid(string $value): bool {
            return \boolval(\preg_match('/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/', $value));
        }
    }


